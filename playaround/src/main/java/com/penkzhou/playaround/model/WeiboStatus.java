package com.penkzhou.playaround.model;

/**
 * Created by Administrator on 14-1-17.
 */
public class WeiboStatus {
    private String author;
    private String weibotime;
    private String content;
    private int imgResId;

    public WeiboStatus(String author, String weibotime, String content, int imgResId) {
        this.author = author;
        this.weibotime = weibotime;
        this.content = content;
        this.imgResId = imgResId;
    }

    public int getImgResId() {
        return imgResId;
    }

    public void setImgResId(int imgResId) {
        this.imgResId = imgResId;
    }

    public String getAuthor() {
        return author;
    }

    public String getWeibotime() {
        return weibotime;
    }

    public void setWeibotime(String weibotime) {
        this.weibotime = weibotime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
