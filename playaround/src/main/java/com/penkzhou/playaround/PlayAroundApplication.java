package com.penkzhou.playaround;

import android.app.Application;

import com.avos.avoscloud.AVACL;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.PushService;
import com.penkzhou.playaround.model.Place;
import com.penkzhou.playaround.model.PlaceComment;
import com.penkzhou.playaround.model.PlaceOperation;

/**
 * Created by Administrator on 14-2-9.
 */
public class PlayAroundApplication extends Application {
    public static AVUser currentUser;

    @Override
    public void onCreate() {
        super.onCreate();
        AVOSCloud.useAVCloudCN();
        AVObject.registerSubclass(Place.class);
        AVObject.registerSubclass(PlaceComment.class);
        AVObject.registerSubclass(PlaceOperation.class);
        AVOSCloud.initialize(this, "xd2lo53g4nd3c2709113fldd1h481b49l9tbqpdgd20gn6md", "vcvrx21m8yzj5ona48ziscdfrmbc7wwion9tk5vejld7guhf");
        AVUser.enableAutomaticUser();
        AVACL defaultACL = new AVACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        AVACL.setDefaultACL(defaultACL, true);
        currentUser = AVUser.getCurrentUser();
        AVInstallation.getCurrentInstallation().saveInBackground();
        PushService.setDefaultPushCallback(this, MainActivity.class);

    }
}
