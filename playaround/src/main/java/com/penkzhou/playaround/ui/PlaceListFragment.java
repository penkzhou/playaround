package com.penkzhou.playaround.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.penkzhou.playaround.MainActivity;
import com.penkzhou.playaround.R;
import com.penkzhou.playaround.adapter.PlaceListAdapter;
import com.penkzhou.playaround.model.Place;

import java.util.List;

/**
 * Created by Administrator on 14-2-26.
 */
public class PlaceListFragment extends Fragment implements AdapterView.OnItemClickListener, BDLocationListener {


    private class LoadingLocationTask extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("LocSDK3", "doInBackground");
            if (mLocationClient != null && mLocationClient.isStarted())
                mLocationClient.requestLocation();
            else
                Log.d("LocSDK3", "locClient is null or not started");
            return null;
        }


        @Override
        protected void onPreExecute() {
            Log.d("LocSDK3", "onPreExecute");
            mLocationClient = new LocationClient(getActivity());
            loading.setVisibility(View.VISIBLE);
            LocationClientOption option = new LocationClientOption();
            option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//设置定位模式
            option.setCoorType("bd09ll");//返回的定位结果是百度经纬度，默认值gcj02
            option.setScanSpan(5000);//设置发起定位请求的间隔时间为5000ms
            option.setIsNeedAddress(true);//返回的定位结果包含地址信息
            option.setNeedDeviceDirect(true);//返回的定位结果包含手机机头的方向
            mLocationClient.setLocOption(option);
            mLocationClient.registerLocationListener(PlaceListFragment.this);
            mLocationClient.start();
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            distance = Integer.parseInt(preferences.getString(getString(R.string.preference_distance_key), "50"));
            Log.d("LocSDK3", "onPreExecute--done!!");
        }
    }

    private PlaceListAdapter placeListAdapter;
    private ListView placeList;
    private TextView loadingTextView;
    private LinearLayout loading;
    private LocationClient mLocationClient = null;
    private BDLocation currentLocation, lastLocation;
    private int distance;
    MainActivity mParent;


    public PlaceListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_placelist, container, false);
        placeList = (ListView) rootView.findViewById(R.id.lv_placelist_list);
        loadingTextView = (TextView) rootView.findViewById(R.id.tv_placelist_loading);
        loading = (LinearLayout) rootView.findViewById(R.id.layout_placelist_loading);
        new LoadingLocationTask().execute();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = (MainActivity) activity;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Place choosePlace = placeListAdapter.getItem(position);
        goToDetail(choosePlace);
    }

    public void goToDetail(Place place) {
        Intent intent = new Intent(getActivity(), PlaceDetailActivity.class);
        intent.putExtra("placeId", place.getObjectId());
        startActivity(intent);
    }

    @Override
    public void onReceiveLocation(BDLocation bdLocation) {
        if (bdLocation != null) {
            currentLocation = bdLocation;
            mLocationClient.stop();
            AVGeoPoint geoPoint = new AVGeoPoint(currentLocation.getLatitude(), currentLocation.getLongitude());
            AVQuery<Place> query = new AVQuery<Place>("Place");
            query.whereWithinKilometers(Place.PLACELOCATION, geoPoint, distance);
            query.orderByAscending("createdAt");
            Log.d("distance", distance + "");
            Log.d("play_cache", query.hasCachedResult() + "");
            query.setCachePolicy(AVQuery.CachePolicy.CACHE_ELSE_NETWORK);
            query.findInBackground(new FindCallback<Place>() {
                public void done(List<Place> avObjects, AVException e) {
                    if (e == null) {
                        Log.d("成功", "查询到" + avObjects.size() + " 条符合条件的数据");
                        placeListAdapter = new PlaceListAdapter(getActivity(), R.layout.place_list_item, avObjects);
                        placeList.setAdapter(placeListAdapter);
                        placeList.setOnItemClickListener(PlaceListFragment.this);
                        loading.setVisibility(View.GONE);
                    } else {
                        Log.d("失败", "查询错误: " + e.getMessage());
                    }
                }
            });
        }
    }

    @Override
    public void onReceivePoi(BDLocation bdLocation) {

    }
}
