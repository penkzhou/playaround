package com.penkzhou.playaround.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;

import com.google.analytics.tracking.android.EasyTracker;
import com.penkzhou.playaround.OnProfileDataPass;

/**
 * Created by Administrator on 14-3-1.
 */
public class ProfileActivity extends ActionBarActivity implements OnProfileDataPass {

    private ActionBar mActionBar;
    private String currentUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        currentUserId = intent.getStringExtra("objectId");
        init();
    }

    public void init() {
        mActionBar = getSupportActionBar();
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(true);
        Tab tab = mActionBar.newTab()
                .setText("个人资料").setTabListener(new TabListener<ProfileFragment>(this, "profile", ProfileFragment.class));
        mActionBar.addTab(tab);
        tab = mActionBar.newTab()
                .setText("喜欢的地点").setTabListener(new TabListener<LikedPlaceListFragment>(this, "profile", LikedPlaceListFragment.class));
        mActionBar.addTab(tab);
        tab = mActionBar.newTab()
                .setText("去过的地点").setTabListener(new TabListener<VisitedPlaceListFragment>(this, "profile", VisitedPlaceListFragment.class));
        mActionBar.addTab(tab);
    }

    @Override
    public Intent getSupportParentActivityIntent() {
        return getIntent();
    }


    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }


    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }


    public static class TabListener<T extends Fragment> implements ActionBar.TabListener {
        private Fragment mFragment;
        private final Activity mActivity;
        private final String mTag;
        private final Class<T> mClass;

        /**
         * Constructor used each time a new tab is created.
         *
         * @param activity The host Activity, used to instantiate the fragment
         * @param tag      The identifier tag for the fragment
         * @param clz      The fragment's Class, used to instantiate the fragment
         */
        public TabListener(Activity activity, String tag, Class<T> clz) {
            mActivity = activity;
            mTag = tag;
            mClass = clz;
        }

    /* The following are each of the ActionBar.TabListener callbacks */

        public void onTabSelected(Tab tab, FragmentTransaction ft) {
            // Check if the fragment is already initialized
            if (mFragment == null) {
                // If not, instantiate and add it to the activity
                mFragment = Fragment.instantiate(mActivity, mClass.getName());
                ft.add(android.R.id.content, mFragment, mTag);
            } else {
                // If it exists, simply attach it in order to show it
                ft.attach(mFragment);
            }
        }

        public void onTabUnselected(Tab tab, FragmentTransaction ft) {
            if (mFragment != null) {
                // Detach the fragment, because another one is being attached
                ft.detach(mFragment);
            }
        }

        public void onTabReselected(Tab tab, FragmentTransaction ft) {
            // User selected the already selected tab. Usually do nothing.
        }
    }
}
