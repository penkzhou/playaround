package com.penkzhou.playaround.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.penkzhou.playaround.MainActivity;
import com.penkzhou.playaround.R;
import com.penkzhou.playaround.adapter.StatusAdapter;
import com.penkzhou.playaround.model.WeiboStatus;

import java.util.ArrayList;

/**
 * Created by Administrator on 14-2-19.
 */
public class ListDemoFragment extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener {

    private StatusAdapter statusAdapter;
    private ArrayList<WeiboStatus> listData;
    private ListView mDrawerListView;
    private String content;
    private int count = 0;
    MainActivity mParent;

    public ListDemoFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = (MainActivity) activity;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.mainlist, container, false);
        mDrawerListView = (ListView) rootView.findViewById(R.id.lvWeibolist);

        listData = new ArrayList<WeiboStatus>();
        content = "今天天气不错，要不我们去划船吧，怎么样，各位，考虑一下哦。哈哈哈哈，是个好的建议吧O(∩_∩)O哈哈~";
        WeiboStatus one = new WeiboStatus("Penkzhou", "3分钟前", content, R.drawable.greenball);
        WeiboStatus two = new WeiboStatus("Penkzhou", "4分钟前", content, R.drawable.greenball);
        WeiboStatus three = new WeiboStatus("Mo_oP", "7分钟前", content, R.drawable.greenball);
        WeiboStatus four = new WeiboStatus("Penkzhou", "25分钟前", content, R.drawable.greenball);
        listData.add(one);
        listData.add(two);
        listData.add(three);
        listData.add(four);
        statusAdapter = new StatusAdapter(getActivity(), R.id.lvWeibolist, listData);
        Button loadBtn = (Button) rootView.findViewById(R.id.btnLoadMore);
        loadBtn.setOnClickListener(this);
        mDrawerListView.setOnItemClickListener(this);
        mDrawerListView.setAdapter(statusAdapter);
        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getActivity(), position + "", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        WeiboStatus newWeibo = new WeiboStatus("周杰伦", count + "分钟前", content, R.drawable.greenball);
        listData.add(newWeibo);
        statusAdapter.notifyDataSetChanged();
        count++;
    }
}
