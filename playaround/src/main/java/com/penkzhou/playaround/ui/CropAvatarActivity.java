package com.penkzhou.playaround.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.ProgressCallback;
import com.avos.avoscloud.SaveCallback;
import com.penkzhou.playaround.R;
import com.penkzhou.playaround.adapter.CropOptionAdapter;
import com.penkzhou.playaround.model.CropOption;
import com.penkzhou.playaround.util.TextProgressBar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CropAvatarActivity extends Activity implements DialogInterface.OnClickListener, View.OnClickListener {
    private Uri mImageCaptureUri;
    private ImageView mImageView;
    private Button cropButton, uploadButton;
    private AlertDialog dialog;
    private TextProgressBar tpb;
    private AVUser user;
    private Bitmap photo;
    private File resultFile;

    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);
        final String[] items = new String[]{"Take from camera", "Select from gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Image");
        builder.setAdapter(adapter, this);
        dialog = builder.create();
        cropButton = (Button) findViewById(R.id.btn_crop);
        uploadButton = (Button) findViewById(R.id.btn_upload);
        mImageView = (ImageView) findViewById(R.id.iv_photo);
        tpb = (TextProgressBar) findViewById(R.id.progressBarWithText);
        cropButton.setOnClickListener(this);
        uploadButton.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;
        switch (requestCode) {
            case PICK_FROM_CAMERA:
                doCrop();
                break;
            case PICK_FROM_FILE:
                mImageCaptureUri = data.getData();
                doCrop();
                break;
            case CROP_FROM_CAMERA:
                Bundle extras = data.getExtras();
                String selectedImagePath;
                if (extras != null) {
                    photo = extras.getParcelable("data");
                    mImageView.setImageBitmap(photo);
                    selectedImagePath = String.valueOf(System.currentTimeMillis())
                            + ".jpg";

                    Log.i("TAG", "new selectedImagePath before file "
                            + selectedImagePath);

                    resultFile = new File(Environment.getExternalStorageDirectory(),
                            selectedImagePath);

                    try {
                        resultFile.createNewFile();
                        FileOutputStream fos = new FileOutputStream(resultFile);
                        photo.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this,
                                "Sorry, Camera Crashed-Please Report as Crash A.",
                                Toast.LENGTH_LONG).show();
                    }
                }
                File f = new File(mImageCaptureUri.getPath());
                if (f.exists()) f.delete();
                uploadButton.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void doCrop() {
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(this, "Can not find image crop app", Toast.LENGTH_SHORT).show();
            return;
        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("outputX", 200);
            intent.putExtra("outputY", 200);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);
            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);
                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                startActivityForResult(i, CROP_FROM_CAMERA);
            } else {
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();
                    co.title = getPackageManager().getApplicationLabel(res.activityInfo.applicationInfo);
                    co.icon = getPackageManager().getApplicationIcon(res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);
                    co.appIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    cropOptions.add(co);
                }
                CropOptionAdapter adapter = new CropOptionAdapter(getApplicationContext(), cropOptions);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        startActivityForResult(cropOptions.get(item).appIntent, CROP_FROM_CAMERA);
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        if (mImageCaptureUri != null) {
                            getContentResolver().delete(mImageCaptureUri, null, null);
                            mImageCaptureUri = null;
                        }
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Intent intent;
        switch (which) {
            case 0://pick from camera
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
                        "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
                intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                try {
                    intent.putExtra("return-data", true);
                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            case 1: //pick from file
                intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_crop:
                dialog.show();
                break;
            case R.id.btn_upload:
                if (photo != null) {
                    AVFile file = null;
                    tpb.setVisibility(View.VISIBLE);
                    try {
                        file = AVFile.withFile("avatar.png", resultFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    user = AVUser.getCurrentUser();
                    user.put("avatarUrl", file);
                    file.saveInBackground(new SaveCallback() {
                                              @Override
                                              public void done(AVException e) {
                                                  user.saveInBackground(new SaveCallback() {
                                                      @Override
                                                      public void done(AVException e) {
                                                          Toast.makeText(getBaseContext(), "图像上传成功", Toast.LENGTH_LONG).show();
                                                          tpb.setVisibility(View.GONE);
                                                      }
                                                  });
                                              }
                                          }, new ProgressCallback() {
                                              @Override
                                              public void done(Integer integer) {
                                                  tpb.setProgress(integer);
                                                  tpb.setText("Loading " + integer + "%");
                                              }
                                          }
                    );
                } else {
                    Toast.makeText(getBaseContext(), "上传的图像不能为空", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

}
