package com.penkzhou.playaround.ui;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.penkzhou.playaround.R;

/**
 * Created by Administrator on 14-2-28.
 */
public class ApplicationSettingFragment extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
    }
}
