package com.penkzhou.playaround.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVStatus;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;
import com.penkzhou.playaround.MainActivity;
import com.penkzhou.playaround.R;
import com.penkzhou.playaround.util.StringUtil;

public class NewStatusFragment extends Fragment implements View.OnClickListener {
    private EditText newstatusEditText;
    private Button sendButton;
    private TextView usernameTextView;
    private View rootView;
    private String statusString;
    private ProgressDialog pdl;
    MainActivity mParent;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_newstatus, container, false);
        init();
        return rootView;
    }

    public void init() {
        usernameTextView = (TextView) rootView.findViewById(R.id.tv_newstatus_username);
        usernameTextView.setText(AVUser.getCurrentUser().getUsername());
        newstatusEditText = (EditText) rootView.findViewById(R.id.etNewstatusContent);
        pdl = new ProgressDialog(getActivity());

        sendButton = (Button) rootView.findViewById(R.id.btn_newstatus_send);
        sendButton.setOnClickListener(this);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = (MainActivity) activity;
    }


    @Override
    public void onClick(View v) {
        statusString = newstatusEditText.getText().toString();
        if (StringUtil.isBlank(statusString)) {
            Toast.makeText(getActivity(), getText(R.string.newstatus_stringcheck_nullstatus), Toast.LENGTH_LONG).show();
            return;
        }
        AVStatus status = new AVStatus();
        status.setMessage(statusString);
        pdl.setMessage(getText(R.string.newstatus_progress_sending));
        pdl.show();
        AVStatus.sendStatusToFollowersInBackgroud(status, new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (e == null) {
                    pdl.dismiss();
                    Log.i("new_status", "Send status finished.");
                    Toast.makeText(getActivity(), getText(R.string.newstatus_text_sendsuccess), Toast.LENGTH_LONG).show();
                    //跳转至动态页面
                    Fragment statusList = new StatusListFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, statusList);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else {
                    Log.i("new_status", e.toString());
                }
            }
        });
    }
}
