package com.penkzhou.playaround.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.penkzhou.playaround.R;
import com.penkzhou.playaround.model.Place;

import java.util.List;

/**
 * Created by Administrator on 14-1-17.
 */
public class PlaceListAdapter extends ArrayAdapter<Place> {
    private Context activity;
    private List<Place> objects;
    private final static int[] typeIcons = {R.drawable.basketball, R.drawable.table_tennis,
            R.drawable.tennis, R.drawable.football, R.drawable.badminton, R.drawable.snooker,
            R.drawable.volleyball, R.drawable.running, R.drawable.swimming, R.drawable.climbing, R.drawable.rockclimbing};

    public static class ViewHolder {
        public TextView placeName, placeAddress, placeDescribe, placeLikeTimes, placeVisitTimes, placeCommentTimes;
        public ImageView logo;

    }

    public PlaceListAdapter(Context context, int resource, List<Place> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;
        if (v == null) {
            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.place_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.placeName = (TextView) v.findViewById(R.id.tv_placelist_name);
            viewHolder.placeAddress = (TextView) v.findViewById(R.id.tv_placelist_distance);
            viewHolder.placeDescribe = (TextView) v.findViewById(R.id.tv_placelist_desc);
            viewHolder.placeLikeTimes = (TextView) v.findViewById(R.id.tv_placelist_like);
            viewHolder.placeVisitTimes = (TextView) v.findViewById(R.id.tv_placelist_visit);
            viewHolder.placeCommentTimes = (TextView) v.findViewById(R.id.tv_placelist_comment);
            viewHolder.logo = (ImageView) v.findViewById(R.id.iv_placelist_logo);
            v.setTag(viewHolder);
        } else viewHolder = (ViewHolder) v.getTag();

        final Place place = objects.get(position);

        if (place != null) {
            viewHolder.placeName.setText(place.getPlaceName());
            viewHolder.placeAddress.setText(place.getPlaceAddress());
            viewHolder.placeDescribe.setText(place.getPlaceDescribe());
            viewHolder.placeLikeTimes.setText(String.valueOf(place.getPlaceLikeTimes()));
            viewHolder.placeVisitTimes.setText(String.valueOf(place.getPlaceVisitTimes()));
            viewHolder.placeCommentTimes.setText(String.valueOf(place.getPlaceCommentTimes()));
            viewHolder.logo.setImageResource(typeIcons[place.getPlaceType().ordinal()]);
        }
        return v;
    }
}
