package com.penkzhou.playaround.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.penkzhou.playaround.R;
import com.penkzhou.playaround.model.PlaceComment;

import java.util.List;

/**
 * Created by Administrator on 14-1-17.
 */
public class PlaceCommentListAdapter extends ArrayAdapter<PlaceComment> {
    private Context activity;
    private List<PlaceComment> objects;

    public static class ViewHolder {
        public TextView commentName, commentContent;
        public ImageView avatar;

    }

    public PlaceCommentListAdapter(Context context, int resource, List<PlaceComment> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;
        if (v == null) {
            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.place_comment_item, null);
            viewHolder = new ViewHolder();
            viewHolder.commentName = (TextView) v.findViewById(R.id.tv_placedetail_commentor);
            viewHolder.commentContent = (TextView) v.findViewById(R.id.tv_placedetail_commentcontent);
            viewHolder.avatar = (ImageView) v.findViewById(R.id.iv_placedetail_commentor);
            v.setTag(viewHolder);
        } else viewHolder = (ViewHolder) v.getTag();

        final PlaceComment comment = objects.get(position);

        if (comment != null) {
            viewHolder.commentName.setText((comment.getSource()).getUsername());
            viewHolder.commentContent.setText(comment.getContent());
            viewHolder.avatar.setImageResource(R.drawable.no_avatar);
        }
        return v;
    }
}
