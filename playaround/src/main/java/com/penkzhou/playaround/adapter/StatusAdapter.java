package com.penkzhou.playaround.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.penkzhou.playaround.R;
import com.penkzhou.playaround.model.WeiboStatus;

import java.util.ArrayList;

/**
 * Created by Administrator on 14-1-17.
 */
public class StatusAdapter extends ArrayAdapter<WeiboStatus> {
    private Activity activity;
    private ArrayList<WeiboStatus> objects;

    public static class ViewHolder {
        public TextView author;
        public TextView weibotime;
        public TextView content;
        public ImageView imgResId;

    }

    public StatusAdapter(Activity context, int resource, ArrayList<WeiboStatus> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;
        if (v == null) {
            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.weibolistitem, null);
            viewHolder = new ViewHolder();
            viewHolder.author = (TextView) v.findViewById(R.id.tvAuthorName);
            viewHolder.content = (TextView) v.findViewById(R.id.tvWeiboContent);
            viewHolder.weibotime = (TextView) v.findViewById(R.id.tvWeiboTime);
            viewHolder.imgResId = (ImageView) v.findViewById(R.id.ivAvatarImg);
            v.setTag(viewHolder);
        } else viewHolder = (ViewHolder) v.getTag();

        final WeiboStatus weiboStatus = objects.get(position);

        if (weiboStatus != null) {
            viewHolder.author.setText(weiboStatus.getAuthor());
            viewHolder.content.setText(weiboStatus.getContent());
            viewHolder.weibotime.setText(weiboStatus.getWeibotime());
            viewHolder.imgResId.setImageResource(weiboStatus.getImgResId());
        }
        return v;
    }
}
