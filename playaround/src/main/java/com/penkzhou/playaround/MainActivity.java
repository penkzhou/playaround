package com.penkzhou.playaround;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.avos.avoscloud.AVUser;
import com.google.analytics.tracking.android.EasyTracker;
import com.penkzhou.playaround.ui.ApplicationSettingFragment;
import com.penkzhou.playaround.ui.LoginActivity;
import com.penkzhou.playaround.ui.NewPlaceFragment;
import com.penkzhou.playaround.ui.NewStatusFragment;
import com.penkzhou.playaround.ui.PlaceListFragment;
import com.penkzhou.playaround.ui.PlaceListInMapActivity;
import com.penkzhou.playaround.ui.ProfileActivity;
import com.penkzhou.playaround.ui.StatusListFragment;
import com.umeng.analytics.MobclickAgent;
import com.umeng.fb.FeedbackAgent;
import com.umeng.fb.model.UserInfo;

import java.util.Map;

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, ActionBar.OnNavigationListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    public ActionBar actionBar;
    private FeedbackAgent agent;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v("penkzhou_plus", "MainActivity.onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initFeedback();
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        actionBar = getSupportActionBar();
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Log.v("penkzhou_plus", "MainActivity.onNavigationDrawerItemSelected_" + position);
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        onSectionAttached(position);
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new PlaceListFragment();
                break;
            case 1:
                fragment = new NewStatusFragment();
                break;
            case 2:
                fragment = new StatusListFragment();
                break;
            case 3:
                fragment = new NewPlaceFragment();
                break;
        }
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    public void onSectionAttached(int number) {
        Log.v("penkzhou_plus", "MainActivity.onSectionAttached");
        switch (number) {
            case 0:
                mTitle = getString(R.string.title_section1);
                break;
            case 1:
                mTitle = getString(R.string.title_section2);
                break;
            case 2:
                mTitle = getString(R.string.title_section3);
                break;
            case 3:
                mTitle = getString(R.string.title_section4);
                break;
        }
    }

    public void restoreActionBar() {
        Log.v("penkzhou_plus", "MainActivity.restoreActionBar");
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.v("penkzhou_plus", "MainActivity.onCreateOptionsMenu");
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    public void initFeedback() {
        agent = new FeedbackAgent(this);
        // to sync the default conversation. If there is new reply, there
        // will be notification in the status bar. If you do not want
        // notification, you can use
        // agent.getDefaultConversation().sync(listener);
        // instead.
        UserInfo info = agent.getUserInfo();
        Map<String, String> remarks = info.getRemark();
        remarks.put("release", Build.VERSION.RELEASE);
        remarks.put("device", Build.DEVICE);
        remarks.put("display", Build.DISPLAY);
        remarks.put("model", Build.MODEL);
        remarks.put("hardware", Build.HARDWARE);
        info.setRemark(remarks);
        agent.setUserInfo(info);
        agent.sync();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.v("penkzhou_plus", "MainActivity.onOptionsItemSelected_" + item.getTitle().toString());
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_locate:
                Intent mapintent = new Intent(MainActivity.this, PlaceListInMapActivity.class);
                startActivity(mapintent);
                break;
            case R.id.action_feedback:
                agent.startFeedbackActivity();
                break;
            case R.id.action_settings:
                Intent setting = new Intent(MainActivity.this, ApplicationSettingFragment.class);
                startActivity(setting);
                break;
            case R.id.action_profile:
                Intent profile = new Intent(MainActivity.this, ProfileActivity.class);
                profile.putExtra("objectId", AVUser.getCurrentUser().getObjectId());
                startActivity(profile);
                break;
            case R.id.action_logout:
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(R.string.quit)
                        .setMessage(R.string.really_quit)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Stop the activity
                                AVUser.logOut();
                                Log.d("currentUser_logout", AVUser.getCurrentUser().toString());
                                Intent login = new Intent(MainActivity.this, LoginActivity.class);
                                startActivity(login);
                                MainActivity.this.finish();
                            }

                        })
                        .setNegativeButton(R.string.no, null)
                        .show();

                break;

        }

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(int i, long l) {
        //click the spinner list
        Log.v("penkzhou_plus", "MainActivity.onNavigationItemSelected_傻逼" + i);
        Toast.makeText(this, i + "", Toast.LENGTH_LONG).show();
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }


}
